#include "pch.h"
#include "ScriptFunction.h"

bool ScriptFunction::Parse(const std::vector<std::string>& params, Passage* passage)
{
	if (passage)
	{
		m_Passage = passage;
		return true;
	}

	return false;
}
