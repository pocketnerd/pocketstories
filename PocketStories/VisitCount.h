#pragma once
#include "ScriptFunction.h"
class VisitCount : public ScriptFunction
{
public:

	bool Parse(const std::vector<std::string>& params, Passage* passage) override;
	const std::string Execute() override;
};

