#include "pch.h"
#include "Passage.h"
#include "nlohmann/json.hpp"

#include "Functions.h"
#include "ScriptFunction.h"
#include "Names.h"


const bool Passage::Parse(const json& data, std::string& error)
{
	extern const std::string& NAME_ELEMENT;
	extern const std::string& TEXT_ELEMENT;
	extern const std::string& LINKS_ELEMENT;
	extern const std::string& ERROR_VALUE_NOT_FOUND;

	Name = data.value(NAME_ELEMENT, ERROR_VALUE_NOT_FOUND);
	if (Name.compare(ERROR_VALUE_NOT_FOUND) == 0)
	{
		error = "Error: Passage is missing a name! \n";
		return false;
	}

	Text = data.value(TEXT_ELEMENT, ERROR_VALUE_NOT_FOUND);
	if (Text.compare(ERROR_VALUE_NOT_FOUND) == 0)
	{
		error = "Error: text element missing for Passage \n";
		return false;
	}

	if (data.contains(LINKS_ELEMENT))
	{
		if (data[LINKS_ELEMENT].is_array()) //Check to ensure Links element is an array of objects
		{
			if (data[LINKS_ELEMENT].size() >= 1)
			{
				for (size_t index = 0; index < data[LINKS_ELEMENT].size(); index++)
				{
					std::shared_ptr<Link> newLink = std::make_shared<Link>();
					if (!newLink->Parse(data[LINKS_ELEMENT][index], error))
					{
						return false;
					}
					Links.push_back(std::move(newLink));
				}
			}
		}
	}

	return true;
}

std::string Passage::GetText()
{
	std::string ParsedText = Text;
	size_t cur = 0;
	while (cur != std::string::npos)
	{
		cur = ParsedText.find("<<", cur);
		if (cur == std::string::npos)
			break;

		size_t end = cur;
		end = ParsedText.find(">>", cur);
		end += 2;

		std::string functionRaw = ParsedText.substr(cur, end - cur);
		cur = 2;
		end = functionRaw.find("(", cur);
		std::string functionName = functionRaw.substr(cur, end - cur);
		cur = end;
		end = functionRaw.find(")", cur);
		std::string paramsRaw = functionRaw.substr(cur, end - cur);
		std::vector<std::string> params;
		cur = 0;
		end = 0;
		while (cur != std::string::npos)
		{
			end = paramsRaw.find(',', cur);
			if (end == std::string::npos)
			{
				params.push_back(paramsRaw.substr(cur));
				break;
			}
			else
				params.push_back(paramsRaw.substr(cur, end - cur));

			cur = end + 1;
		}

		cur = ParsedText.find(functionRaw, 0);
		end = cur + functionRaw.length();

		auto scriptFunction = Functions::GetScriptFunction(functionName);
		if (scriptFunction)
		{
			if (scriptFunction->Parse(params, this))
			{
				std::string output = scriptFunction->Execute();
				ParsedText.replace(cur, end - cur, output);
			}
		}
	}

	return ParsedText;
}

std::string Passage::DisplayLinks()
{
	std::string combinedLinks = "";
	for (int count = 0; count < Links.size(); count++)
	{
		combinedLinks = combinedLinks + std::to_string(count + 1) + ". " + Links[count]->GetText() + '\n';
	}
	return combinedLinks;
}

bool Passage::GetTargetFromSelection(const int& selection, std::string& target)
{
	if (selection < 1)
		return false;

	target = Links[selection - 1]->GetTargetNode();
	return true;
}

const bool Link::Parse(const json& data, std::string& error)
{
	extern const std::string& TEXT_ELEMENT;
	extern const std::string& TARGET_ELEMENT;
	extern const std::string& ERROR_VALUE_NOT_FOUND;
	
	TargetNode = data.value(TARGET_ELEMENT, ERROR_VALUE_NOT_FOUND);
	if (TargetNode.compare(ERROR_VALUE_NOT_FOUND) == 0)
	{
		error = "Error: target element missing for Link \n";
	}
	Text = data.value(TEXT_ELEMENT, ERROR_VALUE_NOT_FOUND);
	if (Text.compare(ERROR_VALUE_NOT_FOUND) == 0)
	{
		error = "Error: text element missing for Link \n";
	}

	return true;
}
