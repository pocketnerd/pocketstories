#pragma once
#include <string>
#include <memory>
#include <vector>

class Passage;

class ScriptFunction
{
public:
	virtual bool Parse(const std::vector<std::string>& params, Passage* passage);
	virtual const std::string Execute() { return ""; };

protected:
	Passage* m_Passage;
};

