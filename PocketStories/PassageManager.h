#pragma once
#include <vector>
#include <memory>
#include <string>

class Passage;

class PassageManager
{
public:

	bool Init();
	void Start();

	void DoStoryLoop();

	PassageManager(std::string dataPath);

	std::shared_ptr<Passage> GetPassage(const int& index);
	std::shared_ptr<Passage> GetPassage(const std::string& target);

private:

	std::string DataPath = "";
	std::vector<std::shared_ptr<Passage>> Passages;

	PassageManager() {};
	PassageManager(const PassageManager&);
	const PassageManager& operator=(const PassageManager&);
	const bool ParsePassages(const std::string& data, std::string& error);
};