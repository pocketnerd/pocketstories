#pragma once
#include <string>
#include <memory>
#include <vector>

class ScriptFunction;

class Functions
{
public:
	static void ToLower(std::string& inString);
	static void FindAllOccurances(std::vector<size_t>& vec, std::string data, std::string toSearch);

	static std::shared_ptr<ScriptFunction> GetScriptFunction(std::string name);
};