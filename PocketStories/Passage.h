#pragma once
#include <string>
#include <vector>
#include <memory>
#include "nlohmann/json.hpp"

using json = nlohmann::json;

struct Link
{
public:

	const bool Parse(const json& data, std::string& error);
	std::string& GetText() { return Text; }
	const std::string& GetTargetNode() { return TargetNode; }

private:
	std::string TargetNode = "";
	std::string Text = "";
};

class Passage
{
public:

	const bool Parse(const json& data, std::string& error);
	std::string GetText();
	const std::string& GetName() { return Name; }
	std::string DisplayLinks();
	bool GetTargetFromSelection(const int& selection, std::string& target);
	void IncrementVisitCount() { VisitCount++; }
	const int& GetVisitCount() { return VisitCount; }

private:
	std::string Name = "";
	std::string Text = "";
	std::vector<std::shared_ptr<Link>> Links;
	int VisitCount = 0;
};