#include "pch.h"
#include "Functions.h"
#include <algorithm>
#include "VisitCount.h"

void Functions::ToLower(std::string& inString)
{
	std::for_each(inString.begin(), inString.end(), [](char& c) {
		c = ::tolower(c);
	});
}

void Functions::FindAllOccurances(std::vector<size_t>& vec, std::string data, std::string toSearch)
{
	// Get the first occurrence
	size_t pos = data.find(toSearch);

	// Repeat till end is reached
	while (pos != std::string::npos)
	{
		// Add position to the vector
		vec.push_back(pos);

		// Get the next occurrence from the current position
		pos = data.find(toSearch, pos + toSearch.size());
	}
}

std::shared_ptr<ScriptFunction> Functions::GetScriptFunction(std::string name)
{
	std::shared_ptr<ScriptFunction> function = nullptr;

	if (name.compare("VisitCount") == 0)
		function = std::make_shared<VisitCount>();

	return function;
}
