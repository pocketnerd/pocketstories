#include "pch.h"
#include "VisitCount.h"
#include "Passage.h"

bool VisitCount::Parse(const std::vector<std::string>& params, Passage* passage)
{
	return ScriptFunction::Parse(params, passage);
}

const std::string VisitCount::Execute()
{
	if (m_Passage)
	{
		auto count = m_Passage->GetVisitCount();
		return std::to_string(count);
	}

	return "";
}
