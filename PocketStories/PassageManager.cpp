#include "pch.h"
#include <fstream>
#include <iostream>
#include <windows.h>
#include "PassageManager.h"
#include "Functions.h"
#include "Names.h"
#include "Passage.h"

bool PassageManager::Init()
{
	std::ifstream DataFile(DataPath);
	std::string ErrorMessage;
	const std::string Data{ std::istreambuf_iterator<char>(DataFile), std::istreambuf_iterator<char>() };
	if (ParsePassages(Data, ErrorMessage))
	{
		return true;
	}
	else
	{
		std::cout << ErrorMessage.c_str() << std::endl;
		return false;
	}
}

void PassageManager::Start() //Wrapper function for future use (delete if nothing comes of it)
{
	DoStoryLoop();
}

void PassageManager::DoStoryLoop() // Story/Engine Loop
{
	extern const std::string& ERROR_SELECTION;

	bool HasStoryEnded = false;
	auto CurrentPassage = GetPassage(0); //First Passage is always the start of the Game.
	//TODO: Modify to allow for saved games in the future

	while (!HasStoryEnded) //Main Engine Loop
	{
		if (!CurrentPassage)
		{
			std::cout << "Error: Story data is empty! If you got this far something is wrong with parsing. \n";
			break;
		}

		//Display Passage/Node
		std::cout << CurrentPassage->GetText() << "\n";
		auto Links = CurrentPassage->DisplayLinks();
		CurrentPassage->IncrementVisitCount();
		if (Links.compare("") == 0)
		{
			HasStoryEnded = true;
		}
		else
		{
			std::cout << Links;
			std::string Input = "";
			std::cin >> Input;

			Functions::ToLower(Input);
			if (Input.compare("exit") == 0) //Check to see if user wants to exit
			{
				HasStoryEnded = true;
				continue;
			}

			int selection = 0;

			try //Test to see if input is an integer
			{
				selection = std::stoi(Input);
			}
			catch (...)
			{
				std::cout << ERROR_SELECTION;
				continue;
			}

			std::string targetPassage = "";
			//Find passage based on user selection, error if not found
			if (CurrentPassage->GetTargetFromSelection(selection, targetPassage))
			{
				CurrentPassage = GetPassage(targetPassage);
			}
			else
			{
				std::cout << ERROR_SELECTION;
				break;
			}
		}
	}
}

PassageManager::PassageManager(std::string dataPath)
{
	DataPath = dataPath;
}

const bool PassageManager::ParsePassages(const std::string& data, std::string& error)
{
	std::string ParseError = "Error: json root must be named \"passage\" and must contain an array!";

	extern const std::string& PASSAGES_ROOT;

	json jData = json::parse(data);
	if (jData[PASSAGES_ROOT].is_array()) //Test to see if root json object contains array
	{
		if (jData[PASSAGES_ROOT].size() < 1)
		{
			error = "Error: passages object does not contain any elements!";
			return false;
		}

		for (size_t index = 0; index < jData[PASSAGES_ROOT].size(); index++) //Parse data from each element as a Passage node
		{
			std::shared_ptr<Passage> newPassage = std::make_shared<Passage>();
			if (!newPassage->Parse(jData[PASSAGES_ROOT][index], ParseError))
			{
				return false;
			}
			Passages.push_back(std::move(newPassage));
		}
	}
	else
	{
		error = ParseError;
		return false;
	}

	return true;
}

std::shared_ptr<Passage> PassageManager::GetPassage(const int& index)
{
	return Passages[index];
}

std::shared_ptr<Passage> PassageManager::GetPassage(const std::string& target)
{
	for (auto& passage : Passages)
	{
		if (passage->GetName().compare(target) == 0)
			return passage;
	}

	return nullptr;
}
